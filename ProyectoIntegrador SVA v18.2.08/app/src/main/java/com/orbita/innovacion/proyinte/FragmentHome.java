package com.orbita.innovacion.proyinte;

import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;

import de.hdodenhof.circleimageview.CircleImageView;

public class FragmentHome extends Fragment {
    private TextView matricula, nombre, apat, amat;
    private TextView grupo, grado, turno, periodo;
    private CircleImageView image;
    private String IDGoogle;

    private String name, tun, peri, matri, a_p, a_m, grup, grad, link;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_fragment_home, container, false);

        matricula = (TextView) v.findViewById(R.id.txtMatricula);
        nombre = (TextView) v.findViewById(R.id.txtNombre);
        apat = (TextView) v.findViewById(R.id.txtAP);
        amat = (TextView) v.findViewById(R.id.txtAM);
        grupo = (TextView) v.findViewById(R.id.txtGrupo);
        grado = (TextView) v.findViewById(R.id.txtGrado);
        turno = (TextView) v.findViewById(R.id.txtTurno);
        periodo = (TextView) v.findViewById(R.id.txtPeriodo);
        image = (CircleImageView) v.findViewById(R.id.alumno);

        IDGoogle = PreferenceManager.getDefaultSharedPreferences(getActivity()).getString("IDGoogle","");

        DocumentReference miDocRef = FirebaseFirestore.getInstance().document("Escuela/" + IDGoogle);

        miDocRef.addSnapshotListener(new EventListener<DocumentSnapshot>() {
            @Override
            public void onEvent(DocumentSnapshot documentSnapshot, FirebaseFirestoreException e) {
                if(documentSnapshot.exists()){
                    limpiar();

                    name = documentSnapshot.getString("nombre");
                    tun = documentSnapshot.getString("turno");
                    peri = documentSnapshot.getString("Periodo");
                    matri = documentSnapshot.getString("Matricula");
                    a_p = documentSnapshot.getString("Apellido_Paterno");
                    a_m = documentSnapshot.getString("Apellido_Materno");
                    grup = documentSnapshot.getString("grupo");
                    grad = documentSnapshot.getString("grado");
                    link = documentSnapshot.getString("link");

                    llenar();
                }else if(e != null){
                    System.out.println("Error " + e);
                }
            }
        });

        return v;
    }

    private void llenar(){
        matricula.setText(matri);
        nombre.setText(name);
        apat.setText(a_p);
        amat.setText(a_m);
        grupo.setText(grup);
        grado.setText(grad);
        turno.setText(tun);
        periodo.setText(peri);

        Glide.with(this)
                .load(link)
                .into(image);

    }

    private void limpiar(){
        matricula.setText("");
        nombre.setText("");
        apat.setText("");
        amat.setText("");
        grupo.setText("");
        grado.setText("");
        turno.setText("");
        periodo.setText("");
    }

}
